<?

namespace EuroCement\Local;


use Bitrix\Main\Application;
use Bitrix\Main\Context;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use CMain;
use CUser;
use Uplab\Core\Renderer;
use Uplab\Core\Traits\EventsTrait;
use EuroCement\Local\Vacancy;
use CIBlockElement;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 */


class Events
{
	use EventsTrait;

	public static function bindEvents()
	{
		$event = EventManager::getInstance();

		$event->addEventHandler("main", "OnProlog", [self::class, "setGlobalData"]);

		$event->addEventHandler("main", "OnEpilog", [self::class, "redirect404"]);

		// $event->addEventHandler("form", "onAfterResultAdd", [self::class, "formHandler"]);
		// $event->addEventHandler("form", "onAfterResultUpdate", [self::class, "formHandler"]);
		
		$event->addEventHandler("main", "OnEndBufferContent", [self::class, "removeCircularReferences"]);
		
		
		$event->addEventHandler("main", "OnBeforeEventSend", [self::class, "onBeforeEventSend"]);
		
		
		//$event->addEventHandler("main", "OnEndBufferContent", [self::class, "deleteKernelJs"]);
		//$event->addEventHandler("main", "OnEndBufferContent", [self::class, "deleteKernelCss"]);
	}
	
	
	public static function deleteKernelJs(&$content) {
	   global $USER, $APPLICATION;
	   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
	   if($APPLICATION->GetProperty("save_kernel") == "Y") return;
	
	   $arPatternsToRemove = Array(
	      '/<script.+?src=".+?kernel_main\/kernel_main_v1\.js\?\d+"><\/script\>/',
	      '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
	      '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
	      '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
	   );
	
	   $content = preg_replace($arPatternsToRemove, "", $content);
	   $content = preg_replace("/\n{2,}/", "\n\n", $content);
	}

	public static function deleteKernelCss(&$content) {
	   global $USER, $APPLICATION;
	   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
	   if($APPLICATION->GetProperty("save_kernel") == "Y") return;
	
	   $arPatternsToRemove = Array(
	      '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
	      '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
	   );
	
	   $content = preg_replace($arPatternsToRemove, "", $content);
	   $content = preg_replace("/\n{2,}/", "\n\n", $content);
	}		
			
	
	public static function removeCircularReferences(&$content) {
		if (defined('ERROR_404') && ERROR_404!="Y" && (!defined('BX_CRONTAB') || (defined('BX_CRONTAB') && BX_CRONTAB!=true))) {
			global $APPLICATION;
			$replaceLink = $APPLICATION->GetCurDir();
			$content = str_replace('href="'.$replaceLink.'"', 'href="javascript:void(0);"', $content);
		}
	}
		

	public static function setGlobalData()
	{
		Loc::loadMessages(Application::getDocumentRoot() . Helper::DEFAULT_TEMPLATE_PATH . "/lang.php");

		if (Context::getCurrent()->getRequest()->isAdminSection()) {
			self::setAdminGlobalData();
		} else {
			self::setPublicGlobalData();
		}
	}

	private static function setAdminGlobalData()
	{
	}

	private static function setPublicGlobalData()
	{
		// Дополнительные параметры, передаваемые в шаблон
		Renderer::getInstance()->setRenderParams([
			"placeholder" => Helper::TRANSPARENT_PIXEL,
			"authorized"  => false,
			"breakpoints" => [
				"md" => 640,
				"lg" => 990
			],			
			"messages"     => [
				"error" => [
					"required"  => "Обязательное поле",
					"email"     => "Введите корректный e-mail адрес",
					"number"    => "Введите корректное число",
					"url"       => "Введите корректный URL",
					"tel"       => "Введите корректный номер телефона",
					"maxlength" => "This fields length must be < \${1}",
					"minlength" => "This fields length must be > \${1}",
					"min"       => "Minimum value for this field is \${1}",
					"max"       => "Maximum value for this field is \${1}",
					"pattern"   => "Input must match the pattern \${1}",
				],
			],
			"colors" => [
			    "white",
			    "gray",
			    "black",
			    "dark",
			    "primary"				
			],
			"text_helpers" => [
				"backward" => "Назад",
				"forward" => "Вперёд"
			],			
		]);

		$rendererIncludePath = Helper::isDevMode()
			? Helper::DEFAULT_TEMPLATE_PATH . "/frontend/src"
			: Helper::DEFAULT_TEMPLATE_PATH . "/dist";

		$rendererIncludePathFull = Application::getDocumentRoot() . $rendererIncludePath;

		// Здесь можно изменить список путей, с которыми будет инициализирован Twig
		Renderer::getInstance()->setLoaderPaths([
			Application::getDocumentRoot() . $rendererIncludePath,
			Application::getDocumentRoot(),
			"template"  => Application::getDocumentRoot() . Helper::DEFAULT_TEMPLATE_PATH,
			"frontend"  => "{$rendererIncludePathFull}",
			"layout"    => "{$rendererIncludePathFull}/include/layout",
			"atoms"     => "{$rendererIncludePathFull}/include/@atoms",
			"molecules" => "{$rendererIncludePathFull}/include/^molecules",
			"organisms" => "{$rendererIncludePathFull}/include/&organisms",
		]);

		// Настройки для кастомного тега {% view '' %} в шаблонах Twig
		Renderer\View\ViewTokenParser::getInstance()->setPathParams([
			"srcExt"  => "twig",
			"dataExt" => "json",

			"viewsSrc" => "{$rendererIncludePath}/include/%s/%s.%s",
			"replace"  => [
				"~^@~"  => "@atoms/",
				"~^\^~" => "^molecules/",
				"~^&~"  => "&organisms/",
			],
		]);

		// Настройки для кастомного тега {% svg '' %} в шаблонах Twig
		Renderer\Svg\SvgTokenParser::getInstance()->setPathParams([
			"src" => [
				"{$rendererIncludePath}/img/%s.svg",
				"%s",
			],
		]);
	}
	public static function onBeforeEventSend(&$arFields, $arTemplate)
	{

		if ($arFields["RS_FORM_SID"] == "SIMPLE_FORM_2") {
			$defaultEmail = "Belgorod_job@eurocem.ru";
			/*$arFields["VACANCY"] = trim($arFields["VACANCY"]);
			if ($arFields["VACANCY"]) {
				$arVacancyData = explode('|',$arFields["VACANCY"]);
				$idVacancy = (int)$arVacancyData[1];
				$obVacancies = new Vacancy;
				$vacancy = $obVacancies->getByID($idVacancy);	
				$vacancyEmail = $vacancy["UF_EMAIL"];
				if (!empty($vacancyEmail)) {
					$arFields["EMAIL_RECEIVER"] = $vacancyEmail;
				} else {
					$arFields["EMAIL_RECEIVER"] = 'Belgorod_job@eurocem.ru';
				}
			} else
			*/if ($arFields["CITY"]) {
				preg_match_all('|\((.*)\)|Uis', $arFields["CITY"], $bunitsIDs);
				$emails = [];
				$resBU = CIBlockElement::GetList(Array(), ["ID" => $bunitsIDs[1]], false, false, ["ID", "PROPERTY_EMAIL"]);
				while($emailData = $resBU->GetNext()){ 
					if (empty($emailData["PROPERTY_EMAIL_VALUE"])) {
						$emails[] = $defaultEmail;						
					} else {
						$emails[] = $emailData["PROPERTY_EMAIL_VALUE"];
					}
				}
				$uniq_emails = array_unique($emails);
				$list_emails = implode(", ", $uniq_emails);
				$arFields["EMAIL_RECEIVER"] = $list_emails;
			} else {
				$arFields["EMAIL_RECEIVER"] = $defaultEmail;				
			}	
		}
	}
	/*
	public static function formHandler($WEB_FORM_ID, $RESULT_ID)
	{
		if ($WEB_FORM_ID != 2) return;

		$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult, $arAnswer2);
		$data = self::prepareFormAnswers($arAnswer);

		die("<pre>" . print_r(compact("data"), true));
	}

	private static function prepareFormAnswers($arAnswer)
	{
		$data = [];

		foreach ($arAnswer as $code => $answer) {
			$answerItem = current($answer);

			if ($answerItem["FIELD_TYPE"] == "file") {
				$value = CFile::GetPath($answerItem["USER_FILE_ID"]);
				$value = $value ? Helper::makeDomainUrl($value) : "";
			} else {
				$value = $answerItem["USER_TEXT"];
			}

			if (!empty($value)) {
				$data[$code] = $value;
			}
		}

		return $data;
	}
	*/
}
