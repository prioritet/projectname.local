<?
/*
	define("BUSINES_IBLOCK", "HH. Бизнес-единицы");
	define("CITIES_IBLOCK", "HH. Города");
	define("DIRECTIONS_IBLOCK", "HH. Направления/Сферы");
	define("EMPLOYMENTS_IBLOCK", "HH. Тип занятости");
	define("EXPIRIENSES_IBLOCK", "HH. Опыт работы");
	define("SCHEDULES_IBLOCK", "HH. График работы");
*/		
namespace EuroCement\Local;

use CIBlockElement;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;
use CFormAnswer;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();


class Sync
{
	public static function _do()
	{
		
		$obVacancies = new Vacancy;
		
		$busineses = self::getBusineses();
		
		$cities = self::getHandbook(CITIES_IBLOCK);
		$directions = self::getHandbook(DIRECTIONS_IBLOCK);
		$employments = self::getHandbook(EMPLOYMENTS_IBLOCK);
		$expirienses = self::getHandbook(EXPIRIENSES_IBLOCK);
		$schedules = self::getHandbook(SCHEDULES_IBLOCK);
		$bunits = self::getHandbook(BUNITS_IBLOCK);
		$vacancies = [];
		foreach($busineses as $busines) {
			$hh = new HH(
				$busines["PROPERTY_CLIENT_ID_VALUE"], 
				$busines["PROPERTY_CLIENT_SECRET_VALUE"], 
				$busines["XML_ID"], 
				$busines["PROPERTY_ACCESS_TOKEN_VALUE"]
			);
			$result_vacancies = $hh->getVacancies();
			if (!isset($vacancies["errors"])) {
				$vacancies = array_merge($vacancies, $result_vacancies);
			}
		}
		if (!empty($vacancies)) {
			
			self::udpateInfoblock($vacancies, $cities, "area", CITIES_IBLOCK);
			self::udpateInfoblock($vacancies, $directions, "specializations", DIRECTIONS_IBLOCK);
			self::udpateInfoblock($vacancies, $employments, "employment", EMPLOYMENTS_IBLOCK);
			self::udpateInfoblock($vacancies, $expirienses, "experience", EXPIRIENSES_IBLOCK);
			self::udpateInfoblock($vacancies, $schedules, "schedule", SCHEDULES_IBLOCK);
			self::udpateInfoblock($vacancies, $bunits, "bunit", BUNITS_IBLOCK);
			
			$old_vacancies = $obVacancies->filterVacancy();
			
			$id_old_vacancies = [];
			foreach($old_vacancies as $old_vacancy) {
				$id_old_vacancies[$old_vacancy["ID"]] = $old_vacancy["ID"];
			}
	
			foreach($vacancies as $vacancy) {
				$db_inner_id = $obVacancies->updateVacancy(
					$vacancy,
					[
						"BUSINES" => $busineses,
						"CITY" => $cities,
						"SPHERES" => $directions,
						"EMPLOYMENT" => $employments,
						"EXPERIENCE" => $expirienses,
						"SCHEDULE" => $schedules,
						"BUNITS" => $bunits
					]
				);
				unset($id_old_vacancies[$db_inner_id]);
			}
			
			foreach($id_old_vacancies as $id_old_vacancy) {
				$obVacancies->disableVacancy($id_old_vacancy);
			}
		
		}
		
		self::syncFormAnswer();
		self::updateCounters();
	}
	
	public static function updateCounters() {
		$directions = self::getHandbook(DIRECTIONS_IBLOCK);
		$obVacancy = new Vacancy;
		foreach($directions as $direction) {
			$fullCount = $obVacancy->filterVacancy(["UF_SPHERES" => $direction["ID"], "UF_ACTIVE" => true],[],true);
			CIBlockElement::SetPropertyValuesEx($direction["ID"], false, array("COUNT" => $fullCount));
		}
		
	}
	
	public static function syncFormAnswer() {
		Loader::includeModule("form");
		$arBUnits = [];
		
		$QUESTION_ID = 9;
		
		$rsAnswers = CFormAnswer::GetList(
			$QUESTION_ID, 
			$by="s_id", 
			$order="desc", 
			[], 
			$is_filtered
		);
		$arExAnswers = [];
		while ($arAnswer = $rsAnswers->Fetch()) {
			$arExAnswers[$arAnswer["ID"]] = $arAnswer;
		}
		
		$arCheckingBunits = [];
		
		$busineses = self::getHandbook(BUNITS_IBLOCK);
		foreach($busineses as $busines) {
			$arCheckingBunits[$busines["ID"]] = $busines["ID"];
			$arBUnits[$busines["ID"]] = [
				"id"    => $busines["ID"],
				"text"  => $busines["NAME"],
				"value" => $busines["ID"],
			];
		}
		
		foreach($arExAnswers as $arExAnswer) {
			if (!in_array($arExAnswer["VALUE"], $arCheckingBunits)) {
				$arFields = array(
				    "ACTIVE" => "N",
				);
				CFormAnswer::Set($arFields, $arExAnswer["ID"]);		
			} else {
				$arFields = array(
				    "ACTIVE"  => "Y",
					"MESSAGE" => htmlspecialchars_decode($arBUnits[$arExAnswer["VALUE"]]["text"]),
				);
				CFormAnswer::Set($arFields, $arExAnswer["ID"]);			
			}
			unset($arCheckingBunits[$arExAnswer["VALUE"]]);
		}
		$i = 0;
		foreach($arCheckingBunits as $arBUnitID) {
			$arBUnit = $arBUnits[$arBUnitID];
			$i++;
			$arFields = array(
			    "QUESTION_ID" => $QUESTION_ID,
			    "MESSAGE"     => htmlspecialchars_decode($arBUnit["text"]),
			    "VALUE"       => $arBUnit["id"],
			    "ACTIVE"      => "Y",
				"FIELD_TYPE"  => "multiselect",
				"C_SORT"      => $i*100,
		    );
			CFormAnswer::Set($arFields);			
		}
		
	}
	
	//получаем список организаций с которыми проводится интеграция с HH
	public static function getBusineses() 
	{
		$arResult = [];
		$rsList = CIBlockElement::GetList(
			[],
			[
				"IBLOCK_ID" => BUSINES_IBLOCK,
				"ACTIVE" => "Y"
			],
			false,
			false,
			["ID","XML_ID", "NAME", "PROPERTY_CLIENT_ID", "PROPERTY_CLIENT_SECRET", "PROPERTY_ACCESS_TOKEN"]
		);
		
		while($arItem = $rsList->GetNext()) {
			$arResult[] = $arItem;
		}
		
		return $arResult;
	}
	
	//получаем значения справочников
	public static function getHandbook(int $iblock_id, $custom_name = false) {
		if ($iblock_id > 0) {
			$arResult = [];
			$rsList = CIBlockElement::GetList(
				[
					"SORT" => "ASC",
					"NAME" => "ASC"
				],
				[
					"IBLOCK_ID" => $iblock_id,
					"ACTIVE" => "Y"
				],
				false,
				false,
				["ID","XML_ID", "NAME", "PROPERTY_NAME_SITE"]
			);
			
			while($arItem = $rsList->GetNext()) {
				if ($custom_name) {
					$arItem["NAME"] = $arItem["PROPERTY_NAME_SITE_VALUE"];
				}
				$arResult[] = $arItem;
			}
			
			return $arResult;			
		}
	}
	
	//собираем уникальные значения и обновляем инфоблок
	public static function udpateInfoblock(array &$vacancies, array &$checking_values, string $type, int $iblock_id) {
		
		$uniq_values = [];
		foreach($vacancies as $vacancy) {
			if (isset($vacancy[$type]["id"])) {
				$uniq_values[$vacancy[$type]["id"]] = $vacancy[$type]["name"];
			} else {
				foreach($vacancy[$type] as $uniq_vacancy_value) {
					if (isset($uniq_vacancy_value["profarea_id"])) {
						$uniq_values[$uniq_vacancy_value["profarea_id"]] = $uniq_vacancy_value["profarea_name"];				
					} else {
						$uniq_values[$uniq_vacancy_value["id"]] = $uniq_vacancy_value["name"];						
					}
				}
			}
		}
		
		self::checkValueHandbook($iblock_id, $uniq_values, $checking_values);
				
	}
	
	//проверяем значение в списке, $values - может принимать как одно значение, так и массив
	public static function checkValueHandbook(int $iblock_id, array $values, &$arItems) {
		
		$arOldItems = [];
		foreach($arItems as $arItem) {
			$arOldItems[$arItem["XML_ID"]] = $arItem["ID"];
		}
		
		$exObValues = CIBlockElement::GetList(
				[],
				[
					"IBLOCK_ID" => $iblock_id
				],
				false,
				false,
				["ID", "XML_ID"]
			);
		$arExValues = [];
		while($arExValue = $exObValues->GetNext()) {
			$arExValues[$arExValue["XML_ID"]] = $arExValue["ID"];
		}
		
		foreach($values as $xml_id => $name) {
			unset($arOldItems[$xml_id]);
			
			$el = new CIBlockElement;

			if (isset($arExValues[$xml_id])) {
				$el->Update($arExValues[$xml_id], [
					"NAME" => $name,
					"ACTIVE" => "Y"
				]);				
			} else {
				if ($iblock_id == DIRECTIONS_IBLOCK || $iblock_id == BUNITS_IBLOCK) {
					$element_id = $el->Add([
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID" => $iblock_id,
						"NAME" => $name,
						"XML_ID" => $xml_id,
						"ACTIVE" => "Y",
						"PROPERTY_VALUES" => [
							"NAME_SITE" => $name
						]
					]);					
				} else {
					$element_id = $el->Add([
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID" => $iblock_id,
						"NAME" => $name,
						"XML_ID" => $xml_id,
						"ACTIVE" => "Y"
					]);
					
				}
				$arItems[] = [
					"ID" => $element_id,
					"NAME" => $name,
					"XML_ID" => $xml_id
				];
			}
		}
		foreach($arOldItems as $iOldID) {
			$el = new CIBlockElement;
			$el->Update($iOldID, [
				"ACTIVE" => "N"
			]);
		}
		
	}
	
	//функция обновления access token
	public static function _updateTokens() {
		
		$busineses = self::getBusineses();
		foreach($busineses as $busines) {
			$hh = new HH(
				$busines["PROPERTY_CLIENT_ID_VALUE"], 
				$busines["PROPERTY_CLIENT_SECRET_VALUE"], 
				$busines["XML_ID"], 
				$busines["PROPERTY_ACCESS_TOKEN_VALUE"]
			);
			$access_token = $hh->getNewAccessToken();
			if (isset($access_token["access_token"])) {
				CIBlockElement::SetPropertyValuesEx($busines["ID"], null, ["ACCESS_TOKEN" => $access_token["access_token"]]);
			}
		}
	}

}