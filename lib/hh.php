<?
namespace EuroCement\Local;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();


class HH
{
	
	private $url = "https://api.hh.ru/";
	
	private $auth_url = "https://hh.ru/oauth/token";
	
	private $email_dev = 'dev@eurocement.ru';
	
	private $app_name = 'EuroCement/1.0';
	
	private $client_id;
	private $client_secret;
	private $access_token;
	private $employer_id;
	
	private $methods = [
		"vacancies" => "vacancies",
		"vacancies/" => "vacancies/"	
	];
	
	public function __construct($client_id, $client_secret, $employer_id, $access_token = '')
	{
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
		$this->employer_id = $employer_id;
		$this->access_token = $access_token;

	}
	
	private function _query(string $method, string $type = "GET", array $params) {
		$result = [];
		if (isset($this->methods[$method])) {
		
			$headers = array(
				"User-Agent: ".$this->app_name." (".$this->email_dev.")",
				"Content-Type: application/json",
				"Authorization: Bearer ".$this->access_token
			);
	                 	
			if ($curl = curl_init()) {
			    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);	
			    
			    if ($type == "GET") {
				    if (strpos($this->methods[$method], "/") === false) {
						curl_setopt($curl, CURLOPT_URL, $this->url.$this->methods[$method].'?'.http_build_query($params));
					} else {
						$alternative_params = $params;
						unset($alternative_params[0]);
						curl_setopt($curl, CURLOPT_URL, $this->url.$this->methods[$method].$params[0].http_build_query($alternative_params));
					}
			    }
			    
			    if ($type == "POST") {
					curl_setopt($curl, CURLOPT_URL, $this->url.$this->methods[$method]);	
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));				    
			    }
			    
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
				$out = curl_exec($curl);
				$result = json_decode($out, TRUE);
				curl_close($curl);
			}
		}
		return $result;
	}
	
	public function getVacancies() {
		$result = [];
		$items = [
			"pages" => 1,
			"page" => 0
		];
		while($items["page"] < $items["pages"]) {
			$items = $this->_query("vacancies", "GET", ["employer_id" => $this->employer_id, "page" => $items["page"]++]);
			if (isset($items["errors"])) {
				return $items;
			}
			$items["page"]++;
			$result = array_merge($result, $items["items"]);
		}
		
		foreach($result as $key => &$vacancy) {
			$vacancy = $this->_query("vacancies/", "GET", [$vacancy["id"]]);
			preg_match_all('|Организация\:(.*)\.|Uis', $vacancy["description"], $mathes);
			$businesName = htmlspecialchars_decode(trim(strip_tags($mathes[1][0])));
			$arParams = array("replace_space"=>"-","replace_other"=>"-"); 
			$trans = \Cutil::translit($businesName,"ru",$arParams);
			$vacancy["bunit"] = [
				"id" => $trans,
				"name" => $businesName
			];
			
			if ($trans == "ao-evrobeton") {
				unset($result[$key]);
			}
		}
		
		return $result;
	}
	
	public function getNewAccessToken() {
		$result = [];
		if ($curl = curl_init()) {
		
			$headers = array(
				"User-Agent: ".$this->app_name." (".$this->email_dev.")",
				"Content-Type: application/x-www-form-urlencoded"
			);
		                 	
			$fields = [
				"grant_type" => "client_credentials",
				"client_id" => $this->client_id,
				"client_secret" => $this->client_secret,
			];
		
		    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);	
			curl_setopt($curl, CURLOPT_URL, 'https://hh.ru/oauth/token');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($fields));
			$out = curl_exec($curl);
			$result = json_decode($out, TRUE);
			curl_close($curl);
		}
		return $result;
	}
}