<?
namespace EuroCement\Local;

defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true || die();


use CIBlockElement;
use Cutil;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

class Vacancy
{
	
	private $id_highload = 1;
	
	private $entity;
	
	public function __construct()
	{
		Loader::includeModule("highloadblock");

		$hlblock = HL\HighloadBlockTable::getById($this->id_highload)->fetch(); 
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$this->entity = $entity->getDataClass(); 
	}

	private function getIDsByXML($xml_ids, $values) {
		$result = [];
		
		foreach($xml_ids as $xml_data) {
			$xml_id = $xml_data["profarea_id"];
			foreach($values as $data) {
				if ($data["XML_ID"] == $xml_id) {
					$result[$data["ID"]]=$data["ID"];
					break;
				}
			}
		}
		return $result;
	}
	
	private function getIDbyXML($xml_id, $values) {
		foreach($values as $data) {
			if ($data["XML_ID"] == $xml_id) {
				return $data["ID"];
			}
		}
		return null;
	}
	
	public function updateVacancy($vacancy, $handbooks = []) {
		$entity_data_class = $this->entity; 
		
		$publish_date = explode("T", $vacancy["published_at"]);
		$ar_publish_date = explode("-", $publish_date[0]);
		
		$search_index=[];
		/*if ($vacancy["area"]["name"]) {
			$search_index[] = $vacancy["area"]["name"];
		}
		if ($vacancy["employer"]["name"]) {
			$search_index[] = $vacancy["employer"]["name"];
		}
		if ($vacancy["experience"]["name"]) {
			$search_index[] = $vacancy["experience"]["name"];
		}
		if ($vacancy["employment"]["name"]) {
			$search_index[] = $vacancy["employment"]["name"];
		}
		if ($vacancy["schedule"]["name"]) {
			$search_index[] = $vacancy["schedule"]["name"];
		}
		foreach($vacancy["specializations"] as $profareadata) {
			if ($profareadata["profarea_name"]) {
				$search_index[] = $profareadata["profarea_name"];
			}
		}
		$search_index = array_unique($search_index);*/

		$arTrParams = array("replace_space"=>"-","replace_other"=>"-");
		$codeTranslitName = Cutil::translit($vacancy["name"],"ru",$arTrParams).'-'.$vacancy["id"];
		$data = array(
			"UF_HH_ID" => $vacancy["id"],
			"UF_CODE" => $codeTranslitName,
			"UF_NAME" => $vacancy["name"],
			"UF_ZP" => $vacancy["salary"]["from"].' - '.$vacancy["salary"]["to"],
			"UF_LINK" => $vacancy["alternate_url"],
			"UF_EMAIL" => $vacancy["contacts"]["email"],
			"UF_ACTIVE" => true,
			"UF_DESCRIPTION" => $vacancy["description"],
			"UF_PUBLISHED_AT" => $ar_publish_date[2].'.'.$ar_publish_date[1].'.'.$ar_publish_date[0],
			"UF_CREATED_AT" => date("d.m.Y H:i:s"),
			"UF_UPDATED_AT" => date("d.m.Y H:i:s"),
			"UF_CITY" => $this->getIDbyXML($vacancy["area"]["id"], $handbooks["CITY"]),
			"UF_SPHERES" => $this->getIDsByXML($vacancy["specializations"], $handbooks["SPHERES"]),
			"UF_BUSINES" => $this->getIDbyXML($vacancy["employer"]["id"], $handbooks["BUSINES"]),
			"UF_BUNIT" => $this->getIDbyXML($vacancy["bunit"]["id"], $handbooks["BUNITS"]),
			"UF_EXPERIENCE" => $this->getIDbyXML($vacancy["experience"]["id"], $handbooks["EXPERIENCE"]),
			"UF_EMPLOYMENT" => $this->getIDbyXML($vacancy["employment"]["id"], $handbooks["EMPLOYMENT"]),
			"UF_SCHEDULE" => $this->getIDbyXML($vacancy["schedule"]["id"], $handbooks["SCHEDULE"]),
			"UF_SEARCH" => implode(", ", $search_index)
		);
		
		
		$check_vacancy = $this->filterVacancy(["UF_HH_ID" => $vacancy["id"]]);

		if ($check_vacancy[0]["ID"]) {
			unset($data["UF_CREATED_AT"]);
			$result = $entity_data_class::update($check_vacancy[0]["ID"], $data);
		} else {
			$result = $entity_data_class::add($data);
		}
		
		$dbElement = CIBlockElement::GetList(
			[],
			["PROPERTY_UF_HH_ID" => $result->getId()],
			false,
			false,
			["ID"]
		);
		$arElement = $dbElement->Fetch();

		if (!$arElement) {
			$el = new CIBlockElement;
			$element_id = $el->Add([
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID" => VACANSIES_IBLOCK,
				"NAME" => $vacancy["name"],
				"CODE" => $codeTranslitName,
				"ACTIVE" => "Y",
				//"DETAIL_TEXT" => $vacancy["description"],
				"DETAIL_TEXT" => '',
				"DETAIL_TEXT_TYPE" => "html",
				"PREVIEW_TEXT" => implode(", ", $search_index),
				"PREVIEW_TEXT_TYPE" => "html",
				"PROPERTY_VALUES" => [
					"UF_HH_ID" => $result->getId()
				]
			]);			
		} else {
			$el = new CIBlockElement;
			$el->Update($arElement["ID"], [
				"NAME" => $vacancy["name"],
				"CODE" => $codeTranslitName,
				"ACTIVE" => "Y",
				//"DETAIL_TEXT" => $vacancy["description"],
				"DETAIL_TEXT" => '',
				"DETAIL_TEXT_TYPE" => "html",
				"PREVIEW_TEXT" => implode(", ", $search_index),
				"PREVIEW_TEXT_TYPE" => "html"
			]);
		}
		
		return $result->getId();

	}
	
	public function disableVacancy($id) {
		if ($id > 0) {
			$entity_data_class = $this->entity; 
			$entity_data_class::update($id, ["UF_ACTIVE" => false]);

			$dbElement = CIBlockElement::GetList(
				[],
				[
					"PROPERTY_UF_HH_ID" => $id,
					"IBLOCK_ID" => VACANSIES_IBLOCK
				],
				false,
				false,
				["ID"]
			);
			$arElement = $dbElement->Fetch();
			if ($arElement["ID"]) {
				$el = new CIBlockElement;
				$el->Update($arElement["ID"], [
					"ACTIVE" => "N",
				]);
			}
		}
	}
	
	public function filterVacancy($filter = [], $params = [], $bOnlyCount = false) {
		$result = [];
		$resultSort = [];
		$entity_data_class = $this->entity; 
		
		$paramsQuery = [
		   "select" => array("*"),
		   "filter" => array($filter),			
		];
		
		if ($params["order"]=="date") {
			$paramsQuery["order"] = ["UF_PUBLISHED_AT" => "DESC"];
		}		
		
		if (isset($params["page"]) && isset($params["count"]) && $bOnlyCount === false) {
			$paramsQuery["limit"] = $params["count"];
			$paramsQuery["offset"] = ($params["page"]-1)*$params["count"];
			
		}
		
		if ($bOnlyCount === true) {
			//unset($paramsQuery["filter"]);
			unset($paramsQuery["order"]);
			$paramsQuery['select'] = array('CNT');
			$paramsQuery['runtime'] = array(
				new Entity\ExpressionField('CNT', 'COUNT(*)')
			);
			
		}
		
		$rsData = $entity_data_class::getList($paramsQuery);
		
		while($arData = $rsData->Fetch()){
		   $result[] = $arData;
		   $resultSort[$arData["ID"]] = $arData;
		}

		if ($bOnlyCount === true) {
			return $result[0]["CNT"];		
		} else {	
			
			
			if ($params["order"]!="date" && isset($filter["ID"]) && is_array($filter["ID"])) {
				$sortedResult = [];
				foreach($filter["ID"] as $iResID) {
					$sortedResult[] = $resultSort[$iResID];
				}
				$result = $sortedResult;
			}
			return $result;
		}
	}
	
	public function getByID($id) {
		$result = false;
		if ($id>0) {
			$arVacancies = $this->filterVacancy(["ID" => $id]);
			$result = array_shift($arVacancies);
			
		}
		return $result;
	}
	
	public function getByCode($code, $check_active = false) {
		$result = false;
		if (!empty($code)) {
			$filterArray = ["UF_CODE" => $code];
			if ($check_active) {
				$filterArray["UF_ACTIVE"] = true;
			}
			$arVacancies = $this->filterVacancy($filterArray);
			$result = array_shift($arVacancies);
			if ($result === NULL) {
				$result = false;
			}
		}
		return $result;
	}	


}