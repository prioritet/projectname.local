<?php

$MESS["eurocement.local_MODULE_NAME"] = "[EuroCement] Local";
$MESS["eurocement.local_MODULE_DESCRIPTION"] = "EuroCement. Libraries & Helpful Tools";
$MESS["eurocement.local_MODULE_PARTNER_NAME"] = "Uplab digital agency";
$MESS["eurocement.local_MODULE_PARTNER_URI"] = "http://www.uplab.ru";
$MESS["eurocement.local_MODULE_NO_D7_ERROR"] = "Sorry. You are using unsupported 1C-Bitrix version.";
